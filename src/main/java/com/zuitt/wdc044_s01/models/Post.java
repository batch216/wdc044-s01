package com.zuitt.wdc044_s01.models;


import javax.persistence.*;

@Entity

@Table(name = "posts")
public class Post {
    @Id

    @GeneratedValue
    private  Long id;
    @Column
    private  String title;
    @Column
    private  String content;

    public Post(){};
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    };

    public String getTitle(){
        return this.title;
    }

    public void setTitle(String title){
        this.title = title;
    }

    public String getContent(){
        return this.content;
    }

    public void setContent(String content){
        this.content = content;
    }
}
