package com.zuitt.wdc044_s01.models;

import javax.persistence.*;

@Entity

@Table(name = "users")
public class User {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String username;
    @Column
    private String password;

    User(){}
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public Long getId() { return this.getId(); }
    public String getUsername(){
        return this.username;
    }
    public void setUsername(String username){
        this.username = username;
    }

    public String getPassword(){
        return this.password;
    }

    public void setPassword(String password){
        this.password = password;
    }

}
